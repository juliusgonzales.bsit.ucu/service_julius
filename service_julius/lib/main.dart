import 'package:flutter/material.dart';
import 'package:service_julius/appointment.dart';
import 'package:service_julius/confirm.dart';
import 'package:service_julius/home.dart';
import 'package:service_julius/welcome.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: WelcomePage(),
    );
  }
}