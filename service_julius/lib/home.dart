import 'package:flutter/material.dart';
import 'package:service_julius/appointment.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double sr = MediaQuery.of(context).size.height * .15;
    double sr2 = MediaQuery.of(context).size.height * .13;
    double sr1 = MediaQuery.of(context).size.width * .9;
    double img1 = MediaQuery.of(context).size.height * .5;
    double img2 = MediaQuery.of(context).size.height * .16;
    double text = MediaQuery.of(context).size.height * .025;
    double text1 = MediaQuery.of(context).size.height * .015;
    double text2 = MediaQuery.of(context).size.height * .05;
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  'assets/profile.png',
                  height: text2,
                ),
                SizedBox(
                  width: text1,
                ),
                Text(
                  "Hello, Julius!",
                  style: TextStyle(fontSize: text),
                ),
              ],
            ),
            SizedBox(height: text,),
            Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 8,
                  child: TextFormField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Color.fromARGB(255, 244, 241, 241),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      prefixIcon: Icon(Icons.search),
                      hintText: "Search",
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
            SizedBox(height: text,),
            Row(
              children: [
                Spacer(),
                Image.asset(
                  'assets/1.png',
                  height: img2,
                ),
                Spacer(),
                Image.asset('assets/2.png', height: img2),
                Spacer(),
              ],
            ),
            SizedBox(height: text,),
            Text(
              'Recommended babysitters',
              style: TextStyle(fontSize: text),
            ),
            SizedBox(height: text,),
            Row(
              children: [
                Spacer(),
                Container(
                  color: Color.fromARGB(255, 245, 243, 236),
                  height: sr,
                  width: sr1,
                  child: Row(
                    children: [
                          Spacer(),
                      Image.asset(
                        'assets/pic.png',
                        height: sr2,
                      ),
                          Spacer(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Spacer(),
                          Text(
                            'Karen Q.',
                            style: TextStyle(fontSize: text),
                          ),
                          Spacer(),
                          Text(
                            'Experience 3 years',
                            style: TextStyle(fontSize: text1),
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              Text(
                                '4.8',
                                style: TextStyle(fontSize: text1),
                              ),
                            ],
                          ),
                          Spacer(),
                        ],
                      ),
                      Expanded(
                        flex: 5,
                        child: Container(),
                      ),
                      IconButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Appointment_page(),));
                        },
                        icon: Icon(Icons.arrow_forward_ios_outlined),
                      ),
                    ],
                  ),
                ),
                Spacer(),
              ],
            ),
            SizedBox(height: text,),
            Row(
              children: [
                Spacer(),
                Container(
                  color: Color.fromARGB(255, 245, 243, 236),
                  height: sr,
                  width: sr1,
                  child: Row(
                    children: [
                          Spacer(),
                      Image.asset(
                        'assets/8.png',
                        height: sr2,
                      ),
                          Spacer(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Spacer(),
                          Text(
                            'Jomar G.',
                            style: TextStyle(fontSize: text),
                          ),
                          Spacer(),
                          Text(
                            'Experience 4 years',
                            style: TextStyle(fontSize: text1),
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              Text(
                                '5',
                                style: TextStyle(fontSize: text1),
                              ),
                            ],
                          ),
                          Spacer(),
                        ],
                      ),
                      Expanded(
                        flex: 5,
                        child: Container(),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.arrow_forward_ios_outlined),
                      ),
                    ],
                  ),
                ),
                Spacer(),
              ],
            ),
            SizedBox(height: text,),
            Row(
              children: [
                Spacer(),
                Container(
                  color: Color.fromARGB(255, 245, 243, 236),
                  height: sr,
                  width: sr1,
                  child: Row(
                    children: [
                          Spacer(),
                      Image.asset(
                        'assets/9.png',
                        height: sr2,
                      ),
                          Spacer(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Spacer(),
                          Text(
                            'Karen Q.',
                            style: TextStyle(fontSize: text),
                          ),
                          Spacer(),
                          Text(
                            'Experience 5 years',
                            style: TextStyle(fontSize: text1),
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              Text(
                                '5',
                                style: TextStyle(fontSize: text1),
                              ),
                            ],
                          ),
                          Spacer(),
                        ],
                      ),
                      Expanded(
                        flex: 5,
                        child: Container(),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.arrow_forward_ios_outlined),
                      ),
                    ],
                  ),
                ),
                Spacer(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
