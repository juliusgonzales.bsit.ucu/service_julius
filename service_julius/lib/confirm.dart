import 'package:flutter/material.dart';
import 'package:service_julius/home.dart';

class Confirm_page extends StatelessWidget {
  const Confirm_page({super.key});

  @override
  Widget build(BuildContext context) {
    double sr = MediaQuery.of(context).size.height * .25;
    double sr2 = MediaQuery.of(context).size.height * .13;
    double sr1 = MediaQuery.of(context).size.width * .9;
    double img1 = MediaQuery.of(context).size.height * .5;
    double img2 = MediaQuery.of(context).size.height * .16;
    double text = MediaQuery.of(context).size.height * .025;
    double text1 = MediaQuery.of(context).size.height * .02;
    double text2 = MediaQuery.of(context).size.height * .05;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Icon(
                  Icons.menu,
                  size: text2,
                ),
              ],
            ),
          ),
          Center(
            child: Text(
              'Karen Q.',
              style: TextStyle(fontSize: text),
            ),
          ),
          Center(
            child: Image.asset(
              'assets/pic.png',
              height: sr,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Enter date of Service.',
              style: TextStyle(fontSize: text, fontWeight: FontWeight.w600),
            ),
          ),
          Row(
            children: [
              Spacer(),
              Expanded(
                flex: 7,
                child: TextFormField(
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Color.fromARGB(255, 238, 235, 232),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      suffixIcon: Icon(Icons.arrow_drop_down),
                      hintText: "April 27, 2023"),
                ),
              ),
              Spacer(),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Work Time.',
              style: TextStyle(fontSize: text, fontWeight: FontWeight.w600),
            ),
          ),
          Row(
            children: [
              Spacer(),
              Expanded(
                flex: 7,
                child: TextFormField(
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Color.fromARGB(255, 238, 235, 232),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      hintText: "8AM - 6PM"),
                ),
              ),
              Spacer(),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Location & Address.',
              style: TextStyle(fontSize: text, fontWeight: FontWeight.w600),
            ),
          ),
          Row(
            children: [
              Spacer(),
              Expanded(
                flex: 7,
                child: TextFormField(
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Color.fromARGB(255, 238, 235, 232),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      suffixIcon: Icon(Icons.arrow_drop_down),
                      hintText: "Amamperez villasis Pang."),
                ),
              ),
              Spacer(),
            ],
          ),
          SizedBox(height: text,),
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.only(left: 35, right: 35, top: 20, bottom: 20),
                elevation: 0,
                backgroundColor: Color.fromARGB(255, 238, 235, 232),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage(),
                  ),
                );
              },
              child: Text(
                'Confirm',
                style: TextStyle(color: Colors.black),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
