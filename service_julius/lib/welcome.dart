import 'package:flutter/material.dart';
import 'package:service_julius/log.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double img1 = MediaQuery.of(context).size.height * .5;
    double text = MediaQuery.of(context).size.height * .025;
    double text1 = MediaQuery.of(context).size.height * .015;
    return Scaffold(
      body: Column(
        children: [
          Spacer(),
          Center(
            child: Text(
              "Difficult home office?",
              style: TextStyle(fontSize: text, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: text1,
          ),
          Center(
            child: Text(
              "Quickly find the best babysitter around",
              style: TextStyle(
                fontSize: text1,
              ),
            ),
          ),
          Center(
            child: Text(
              "and word from home peacefully",
              style: TextStyle(
                fontSize: text1,
              ),
            ),
          ),
          Spacer(),
          Image.asset(
            'assets/wel.png',
            height: img1,
          ),
          Spacer(),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              padding: EdgeInsets.all(20),
              backgroundColor: Color.fromARGB(255, 243, 242, 239),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => LoginPage(),
                ),
              );
            },
            child: Text(
              'Find a Baby-Sitter',
              style: TextStyle(color: Colors.black),
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
