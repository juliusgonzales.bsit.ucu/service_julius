import 'package:flutter/material.dart';
import 'package:service_julius/confirm.dart';

class Appointment_page extends StatelessWidget {
  const Appointment_page({super.key});

  @override
  Widget build(BuildContext context) {
    double sr = MediaQuery.of(context).size.height * .15;
    double sr2 = MediaQuery.of(context).size.height * .13;
    double sr1 = MediaQuery.of(context).size.width * .9;
    double img1 = MediaQuery.of(context).size.height * .5;
    double img2 = MediaQuery.of(context).size.height * .16;
    double text = MediaQuery.of(context).size.height * .025;
    double text1 = MediaQuery.of(context).size.height * .02;
    double text2 = MediaQuery.of(context).size.height * .05;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: img1,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/pic.png'), fit: BoxFit.cover),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Karen Q. from Greenland",
              style: TextStyle(fontSize: text),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                Icon(
                  Icons.star_half,
                  color: Colors.amber,
                ),
              ],
            ),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Bio",
              style: TextStyle(fontSize: text),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Karen is an experienced babysitter, withalmost 10years practice. she raised her own2 children....",
              style: TextStyle(fontSize: text1),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Spacer(),
                Column(
                  children: [
                    Container(
                      color: Color.fromARGB(255, 188, 160, 162).withOpacity(.5),
                      height: MediaQuery.of(context).size.height * .08,
                      width: MediaQuery.of(context).size.height * .08,
                      child: Center(
                        child: Image.asset('assets/3.png'),
                      ),
                    ),
                    Text('Fulltime'),
                  ],
                ),
                Spacer(),
                Column(
                  children: [
                    Container(
                      color: Color.fromARGB(255, 188, 160, 162).withOpacity(.5),
                      height: MediaQuery.of(context).size.height * .08,
                      width: MediaQuery.of(context).size.height * .08,
                      child: Center(
                        child: Image.asset('assets/4.png'),
                      ),
                    ),
                    Text('Car'),
                  ],
                ),
                Spacer(),
                Column(
                  children: [
                    Container(
                      color: Color.fromARGB(255, 188, 160, 162).withOpacity(.5),
                      height: MediaQuery.of(context).size.height * .08,
                      width: MediaQuery.of(context).size.height * .08,
                      child: Center(
                        child: Image.asset('assets/5.png'),
                      ),
                    ),
                    Text('Cooking'),
                  ],
                ),
                Spacer(),
                Column(
                  children: [
                    Container(
                      color: Color.fromARGB(255, 188, 160, 162).withOpacity(.5),
                      height: MediaQuery.of(context).size.height * .08,
                      width: MediaQuery.of(context).size.height * .08,
                      child: Center(
                        child: Image.asset('assets/6.png'),
                      ),
                    ),
                    Text('Care'),
                  ],
                ),
                Spacer(),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Free'),
                    Text('P5,000'),
                  ],
                ),
                Spacer(),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor:
                        Color.fromARGB(255, 188, 160, 162).withOpacity(.5),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Confirm_page(),
                      ),
                    );
                  },
                  child: Text(
                    'Set Appointment',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
